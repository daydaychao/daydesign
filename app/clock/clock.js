const secHand = document.querySelector('.sec-hand');
const minHand = document.querySelector('.min-hand');
const hourHand = document.querySelector('.hour-hand');
const clockTime = document.querySelector('.clock-time');

function setDate() {
  const now = new Date();
  const sec = now.getSeconds().toString().padStart(2, '0');
  const secDeg = sec * (360 / 60) + 90;
  const min = now.getMinutes().toString().padStart(2, '0');
  const minDeg = min * (360 / 60) + 90;
  const hour = now.getHours().toString().padStart(2, '0');
  const hourDeg = hour * (360 * (60 / 60 / 12)) + 90;

  secHand.style.transform = `rotate(${secDeg}deg)`;
  minHand.style.transform = `rotate(${minDeg}deg)`;
  hourHand.style.transform = `rotate(${hourDeg}deg)`;
  console.log(`Time:${hour}:${min}:${sec}`);
  console.log(clockTime);
  clockTime.textContent = `${hour}:${min}:${sec}`;
}

setInterval(setDate, 1000);
