const panels = document.querySelectorAll('.panel');

function checkClass() {
  var ele = this;

  for (let panel of panels) {
    // 如果別的視窗有開啟 關閉別的視窗
    if (
      panel.classList.contains('is-opened') &&
      !this.classList.contains('is-opened')
    ) {
      panel.classList.remove('is-opened');
      panel.classList.remove('is-actived');
      console.log('別的視窗有開啟 先關閉');
    }
  }

  // 如果點擊到已開啟視窗 關閉別的視窗
  if (ele.classList.contains('is-opened')) {
    ele.classList.remove('is-opened');
    ele.classList.remove('is-actived');
    console.log('點擊到已開啟視窗 return');
    return;
  }
  toggleOpen(ele);
}

function toggleOpen(ele) {
  ele.classList.toggle('is-opened');
  ele.classList.toggle('is-actived');
  console.log('啟動toggle');

  //如果將未開啟的視窗加上not-opened
  for (let panel of panels) {
  }
}

panels.forEach((panel) => panel.addEventListener('click', checkClass));
