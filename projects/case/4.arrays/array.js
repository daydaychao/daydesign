const inventors = [
  {
    first: 'Koookoo',
    last: 'Loo',
    year: 1278,
    passed: 1330,
  },
  {
    first: 'Yahoo',
    last: 'Yoo',
    year: 1909,
    passed: 2020,
  },
  {
    first: 'Joo',
    last: 'Aero',
    year: 1500,
    passed: 1550,
  },
  {
    first: 'Kao',
    last: 'Leoo',
    year: 1580,
    passed: 1620,
  },
];

const people = [
  'Peter Wu',
  'Willen Lo',
  'Becket Yo',
  'Berlor Rai',
  'Staney Haldason',
];

// 1.[filter] 篩選出要的
const fifteen = inventors.filter(function (inventor) {
  if (inventor.year >= 1500 && inventor.year <= 1600) {
    return true; //keep it
  }
});
console.log(fifteen);

// 2.[map] return a new array  做一些行為放到新的陣列
const fullNames = inventors.map(
  (inventor) => inventor.first + ' ' + inventor.last
);
console.log(fullNames);

// 3.[sort] 排序
const ordered = inventors.sort(function (a, b) {
  if (a.year > b.year) {
    return 1;
  } else {
    return -1;
  }
});
console.log(ordered);

// 3.[sort] 三元運算子 The ternary operator
const orderedB = inventors.sort((a, b) => (a.year > b.year ? 1 : -1));
console.log(orderedB);

// 4.[reduce] build something 重組 >> 相加
const totalYears = inventors.reduce((total, inventor) => {
  return total + (inventor.passed - inventor.year);
}, 0);
console.log(totalYears);

//5.[sort] 由年齡排列發明家
const oldest = inventors.sort(function (a, b) {
  const lastGuy = a.passed - a.year;
  const nextGuy = b.passed - b.year;
  console.log(lastGuy);
  console.log(nextGuy);
  return lastGuy > nextGuy ? -1 : 1;
});
console.table(oldest);

//6. 選出某文章含有'de’
// const category = document.querySelector('.mw-category');

//因為querySelector是nodelist,不是array, map+filter會無法使用
//以下是轉換方式
// const links = Array.from(category.querySelectorAll('a'));
//or
// const links = [...category.querySelectorAll('a')];

//變字串然後尋找de
// const de = links
// .map((link) => link.textContent)
// .filter((streetName) => streetName.includes('de'));

//7. [sort]
const alpha = people.sort(function (lastOne, nextOne) {
  // const parts = lastOne.split(',');
  const [aLast, aFirst] = lastOne.split(', ');
  const [bLast, bFirst] = nextOne.split(', ');
  // console.log(aLast);
  // console.log(bLast);
  return aLast > bLast ? 1 : -1;
});
console.log(alpha);

// 8.[reduce]同種類相加 重組
const data = [
  'car',
  'car',
  'car',
  'walk',
  'ship',
  'car',
  'ship',
  'plane',
  'ship',
  'walk',
  'sleep',
];
const transportation = data.reduce(function (obj, item) {
  console.log(obj);
  console.log(item);
  if (!obj[item]) {
    obj[item] = 0;
  }
  obj[item]++;
  return obj;
}, {});

console.log(transportation);
