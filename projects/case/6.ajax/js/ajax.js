const cities = [];

const search = document.querySelector('.search');
const suggestions = document.querySelector('.suggestions');

//Ajax Fetch Json API
// const endpoint =
//   'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json';
// const prom = fetch(endpoint)
//   .then((blob) => blob.json())
//   .then((data) => cities.push(...data))
//   .then(console.log('json loading done'));

//用本地的Json file
const endpoint = './data.json';
const prom = fetch(endpoint)
  .then((blob) => blob.json())
  .then((data) => cities.push(...data))
  .then(console.log('json loading done'));

search.addEventListener('change', _.debounce(displayMatches, 600));
search.addEventListener('keyup', _.debounce(displayMatches, 600));

//尋找合的array
function findMatches(wordToMatch, cities) {
  const regex = new RegExp(wordToMatch, 'gi');
  return cities.filter((place) => {
    return place.city.match(regex) || place.state.match(regex);
  });
}

//拼湊到畫面
function displayMatches() {
  const matchArray = findMatches(this.value, cities);
  const regex = new RegExp(this.value, 'gi');
  if (this.value == '') {
    suggestions.innerHTML = '';
    return;
  }
  const html = matchArray
    .map((place) => {
      const cityName = place.city.replace(
        regex,
        `<span class="keywords">${this.value}</span>`
      );
      const cityStatue = place.state.replace(
        regex,
        `<span class="keywords">${this.value}</span>`
      );
      return `
      <li>
      <a href="https://zh.wikipedia.org/wiki/${place.city}" target="_blank">
      <i class="material-icons">search</i>
      <span class="city"> 
        ${cityName} ${cityStatue}
      </span>
      <span class="population">${numberWithCommas(place.population)}</span>
      </a>
    </li>
    `;
    })
    .join('');
  suggestions.innerHTML = html;
  suggestions.classList.add('loading-done');
}

//數字三位加上逗號
function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
